import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

let store = new Vuex.Store({
    state: {
        products: [],
        cart: [],
        searchValue: ''
    },
    mutations: {
        SET_PRODUCT_TO_STATE: (state, products) => {
            state.products = products
        },
        SET_CART: (state, product) => {
            if (state.cart.length) {
                let isProductExists = false

                state.cart.forEach( (item) => {
                    if (item.id === product.id) {
                        isProductExists = true
                        item.quantity++
                    }
                })

                if (!isProductExists) {
                    state.cart.push(product)
                }
            }
            else {
                state.cart.push(product)
            }
        },
        REMOVE_FROM_CART: (state, index) => {
            state.cart.splice(index, 1)
        },
        INKREMENT: (state, index) => {
            state.cart[index].quantity++
        },
        DEKREMENT: (state, index) => {
            if (state.cart[index].quantity > 1) {
                state.cart[index].quantity--
            }
        },
        SET_SEARCH_VALUE: (state, value) => {
            state.searchValue = value
        }
    },
    actions: {
        GET_PRODUCT_FROM_API({commit}) {
            return axios('http://localhost:3000/products', {
                method: 'GET'
            })
            .then((products) => {
                commit('SET_PRODUCT_TO_STATE', products.data)
                return products
            })
            .catch((error) => {
                console.log(error)
                return error
            })
        },
        ADD_TO_CART({commit}, product) {
            commit('SET_CART', product)
        },
        DELETE_FROM_CART({commit}, index) {
            commit('REMOVE_FROM_CART', index)
        },
        INKREMENT_CART_ITEM({commit}, index) {
            commit('INKREMENT', index)
        },
        DEKREMENT_CART_ITEM({commit}, index) {
            commit('DEKREMENT', index)
        },
        GET_SEARCH_VALUE({commit}, value) {
            commit('SET_SEARCH_VALUE', value)
        }
    },
    getters: {
        PRODUCTS(state) {
            return state.products
        },
        CART(state) {
            return state.cart
        },
        SEARCH_VALUE(state) {
            return state.searchValue
        }
    }
})

export default store