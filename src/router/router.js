import Vue from 'vue'
import Router from 'vue-router'

import Catalog from '../components/Catalog'
import Cart from '../components/Cart'


Vue.use(Router)

let router = new Router({
    routes: [
        {
            path: '/',
            name: 'Catalog',
            component:  Catalog
        },
        {
            path: '/cart',
            name: 'Cart',
            component:  Cart,
            props: true
        }
    ],
    // mode: history
})

export default router